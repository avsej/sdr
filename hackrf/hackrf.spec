# https://src.fedoraproject.org/rpms/hackrf

%global commit 0335f1a31782c08e2e42a91f46aa117afa5806b7
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           hackrf
Version:        2017.02.1
Release:        1%{?dist}
Summary:        HackRF Utilities

Group:          Development/Libraries
License:        GPLv2
URL:            https://github.com/mossmann/%{name}/wiki
Source0:        https://github.com/mossmann/%{name}/archive/%{commit}/%{name}-%{commit}.tar.gz

BuildRequires:  cmake
BuildRequires:  libusbx-devel
BuildRequires:  fftw3-devel
BuildRequires:  systemd

%description
Hardware designs and software for HackRF, a project to produce a low cost, open
source software radio platform.

%package devel
Requires:       %{name} = %{version}-%{release}%{?isa}
Requires:       libusbx-devel
Summary:        Development files for %{name}
Provides:       %{name}-static = %{version}-%{release}%{?isa}

%package doc
Requires:       %{name} = %{version}-%{release}%{?isa}
Summary:        Supplemental documentation for HackRF
BuildArch:      noarch

%package static
Requires:       %{name}-devel = %{version}-%{release}%{?isa}
Summary:        Static libraries for libhackrf

%description devel
Files needed to develop software against libhackrf.

%description doc
Supplemental documentation for HackRF. For more information, visit the wiki at
https://github.com/mossmann/hackrf/wiki

%description static
Static libraries for libhackrf.

%prep
%setup -qn %{name}-%{commit}

# Fix "plugdev" nonsense
%if 0%{?fedora} >= 20
sed -i -e 's/GROUP="@HACKRF_GROUP@"/ENV{ID_SOFTWARE_RADIO}="1"/g' host/libhackrf/53-hackrf.rules.in
sed -i -e 's/GROUP="plugdev"/ENV{ID_SOFTWARE_RADIO}="1"/g' host/libhackrf/53-hackrf.rules
%else
sed -i -e 's/GROUP="@HACKRF_GROUP@"/TAG+="uaccess"/g' host/libhackrf/53-hackrf.rules.in
sed -i -e 's/GROUP="plugdev"/TAG+="uaccess"/g' host/libhackrf/53-hackrf.rules
%endif

%build
mkdir -p build
pushd build
%cmake ../host \
    -DINSTALL_UDEV_RULES=on \
    -DUDEV_RULES_PATH:PATH=%{_udevrulesdir} \
    -DUDEV_RULES_GROUP=plugdev \

popd

make -C build %{?_smp_mflags}

%install
make -C build install DESTDIR=%{buildroot}

%post
/sbin/ldconfig
%udev_rules_update

%postun
/sbin/ldconfig
%udev_rules_update

%files
%doc COPYING TRADEMARK Readme.md
%{_bindir}/hackrf_*
%{_libdir}/libhackrf.so.*
%{_udevrulesdir}/53-hackrf.rules

%files devel
%{_includedir}/libhackrf/hackrf.h
%{_libdir}/pkgconfig/libhackrf.pc
%{_libdir}/libhackrf.so

%files static
%{_libdir}/libhackrf.a

%files doc
%doc doc/*

%changelog
* Fri Sep 08 2017 Sergey Avseyev <sergey.avseyev@gmail.com> - 2017.02.1-1
- Update package to 2017.02.1

* Wed Aug 02 2017 Fedora Release Engineering <releng@fedoraproject.org> - 2015.07.2-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 2015.07.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 2015.07.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Wed Feb 03 2016 Fedora Release Engineering <releng@fedoraproject.org> - 2015.07.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Mon Aug 24 2015 Scott K Logan <logans@cottsay.net> - 2015.07.2-1
- Update to 2015.07.2

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2014.08.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Tue Sep 23 2014 Scott K Logan <logans@cottsay.net> - 2014.08.1-1
- Initial package
